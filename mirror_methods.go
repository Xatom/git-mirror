package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	CheckingMessage     = "Checking for repositories to mirror"
	ReadErrorMessage    = "Failed to read from %s [%v]"
	StartingMessage     = "Starting mirroring repositories"
	MirroringMessage    = "Mirroring %s: %s [%v]"
	MirroringLfsMessage = "Mirroring LFS %s: %s [%v]"
	DoneMessage         = "Done mirroring %d %s repositories (success: %d, fail: %d)"
	IsBareGitMessage    = "Checking if %s is a bare Git repository"

	StartingStatus = "Starting"
	FailedStatus   = "Failed"
	SuccessStatus  = "Success"

	GitCommand = "git"
)

type GitResult struct {
	Fail    int
	Success int

	LfsSuccess int
	LfsFail    int
}

func runGitCommand(gitPath string, args ...string) (output string, err error) {
	command := exec.Command(GitCommand, args...)
	command.Dir = gitPath

	var out bytes.Buffer
	command.Stdout = &out
	command.Stderr = &out

	err = command.Run()
	output = out.String()

	if verbose {
		log.Print(output)
	}

	return output, err
}

func isGitMirror(gitPath string) bool {
	if verbose {
		log.Printf(IsBareGitMessage, gitPath)
	}

	output, err := runGitCommand(gitPath, "rev-parse", "--is-bare-repository")
	if err != nil {
		return false
	}

	output = strings.TrimSpace(output)
	isGitMirror, _ := strconv.ParseBool(output)

	return isGitMirror
}

func getGitPaths() []string {
	log.Print(CheckingMessage)

	var gitPaths []string

	files, err := ioutil.ReadDir(directory)
	if err != nil {
		log.Fatalf(ReadErrorMessage, directory, err)
	}

	for _, file := range files {
		if !file.IsDir() {
			continue
		}

		gitPath := filepath.Join(directory, file.Name())

		if isGitMirror(gitPath) {
			gitPaths = append(gitPaths, gitPath)
		}
	}

	return gitPaths
}

func mirrorGitLfsRepository(gitPath string, gitResult *GitResult) {
	log.Printf(MirroringLfsMessage, gitPath, StartingStatus, nil)

	_, err := runGitCommand(gitPath, "lfs", "fetch", "--all")

	if err != nil {
		gitResult.LfsFail++
		log.Printf(MirroringLfsMessage, gitPath, FailedStatus, err)
	} else {
		gitResult.LfsSuccess++
		log.Printf(MirroringLfsMessage, gitPath, SuccessStatus, err)
	}
}

func mirrorGitRepository(gitPath string, gitResult *GitResult) {
	log.Printf(MirroringMessage, gitPath, StartingStatus, nil)

	_, err := runGitCommand(gitPath, "remote", "update", "--prune")

	if err != nil {
		gitResult.Fail++
		log.Printf(MirroringMessage, gitPath, FailedStatus, err)
		return
	} else {
		gitResult.Success++
		log.Printf(MirroringMessage, gitPath, SuccessStatus, nil)
	}

	if useLfs {
		mirrorGitLfsRepository(gitPath, gitResult)
	}
}

func mirrorGitRepositories() {
	log.Print(StartingMessage)

	gitPaths := getGitPaths()
	gitResult := GitResult{}

	for _, gitPath := range gitPaths {
		mirrorGitRepository(gitPath, &gitResult)
	}

	log.Printf(DoneMessage, len(gitPaths), "bare", gitResult.Success, gitResult.Fail)

	if useLfs {
		log.Printf(DoneMessage, len(gitPaths), "LFS", gitResult.LfsSuccess, gitResult.LfsFail)
	}
}
