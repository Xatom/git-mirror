package main

import (
	"log"
	"os"
	"strconv"
	"time"
)

const (
	DefaultVersion = "Development"

	VersionLogMessage       = "Running git-mirror @ %s"
	InvalidDirectoryMessage = "Directory '%s' does not exist [%v]"
	InvalidIntervalMessage  = "Interval '%s' is invalid [%v]"
	InvalidUseLfsMessage    = "Use LFS '%s' is invalid [%v]"
	InvalidVerboseMessage   = "Verbose '%s' is invalid [%v]"

	VersionEnvName   = "GIT_MIRROR_VERSION"
	DirectoryEnvName = "GIT_MIRROR_DIRECTORY"
	IntervalEnvName  = "GIT_MIRROR_INTERVAL"
	UseLfsEnvName    = "GIT_MIRROR_USE_LFS"
	VerboseEnvName   = "GIT_MIRROR_VERBOSE"
)

var (
	version   = getVersion()
	directory = getDirectory()
	interval  = getInterval()
	useLfs    = getUseLfs()
	verbose   = getVerbose()
)

func main() {
	logVersions()

	mirrorGitRepositories()

	ticker := time.NewTicker(interval)

	for range ticker.C {
		mirrorGitRepositories()
	}
}

func logVersions() {
	log.Printf(VersionLogMessage, version)

	if verbose {
		runGitCommand(directory, "--version")
		runGitCommand(directory, "lfs", "version")
	}
}

func getVersion() string {
	versionEnv := os.Getenv(VersionEnvName)
	if versionEnv == "" {
		versionEnv = DefaultVersion
	}

	return versionEnv
}

func getDirectory() string {
	directoryEnv := os.Getenv(DirectoryEnvName)

	fileInfo, err := os.Stat(directoryEnv)
	if os.IsNotExist(err) || !fileInfo.IsDir() {
		log.Fatalf(InvalidDirectoryMessage, directoryEnv, err)
	}

	return directoryEnv
}

func getInterval() time.Duration {
	intervalEnv := os.Getenv(IntervalEnvName)

	duration, err := time.ParseDuration(intervalEnv)
	if err != nil || duration <= 0 {
		log.Fatalf(InvalidIntervalMessage, intervalEnv, err)
	}

	return duration
}

func getUseLfs() bool {
	useLfsEnv := os.Getenv(UseLfsEnvName)
	if useLfsEnv == "" {
		return false
	}

	result, err := strconv.ParseBool(useLfsEnv)
	if err != nil {
		log.Fatalf(InvalidUseLfsMessage, useLfsEnv, err)
	}

	return result
}

func getVerbose() bool {
	verboseEnv := os.Getenv(VerboseEnvName)
	if verboseEnv == "" {
		return false
	}

	result, err := strconv.ParseBool(verboseEnv)
	if err != nil {
		log.Fatalf(InvalidVerboseMessage, verboseEnv, err)
	}

	return result
}
