FROM golang:alpine AS build

WORKDIR /go/src/git-mirror
COPY . .

RUN go get -v
RUN go build -v

FROM alpine:latest

WORKDIR /app
COPY --from=build /go/bin/git-mirror .

RUN apk add --no-cache git git-lfs openssh-client
RUN git config --global credential.helper 'store --file ~/.git-credentials'

ARG version=Test

ENV GIT_MIRROR_VERSION=${version}

ENTRYPOINT ["./git-mirror"]
