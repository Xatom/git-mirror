# git-mirror
Small utility to automatically mirror Git repositories on set intervals.

# Usage
To start git-mirror:

```bash
docker pull registry.gitlab.com/xatom/git-mirror/$ARCH:latest
docker stop git-mirror
docker rm git-mirror
docker run -d --name git-mirror --restart always \
    -e GIT_MIRROR_INTERVAL=$INTERVAL \
    -e GIT_MIRROR_DIRECTORY=/repos \
    -v $DIRECTORY:/repos:rw \
    registry.gitlab.com/xatom/git-mirror/$ARCH:latest
```

Make sure to:
* Replace `$ARCH` with the CPU architecture of the host (`amd64` or `arm`)
* Replace `$INTERVAL` with a mirror interval (for example `12h`, [docs](https://golang.org/pkg/time/#ParseDuration))
* Replace `$DIRECTORY` with a path to a directory containing bare Git repositories

The directory structure should be something like this:
```
root
├ first-git-repo
├ second-git-repo
└ third-git-repo
```

**Note:** only bare Git repositories will be mirrored (cloned with the `--bare` or `--mirror` flag).

## Using SSH
To mirror repositories over SSH, add the following to the `docker run` command:

```bash
docker run ...
    -v $SSH_KEY:/root/.ssh/id_rsa:ro \
    -v $KNOWN_HOSTS:/root/.ssh/known_hosts:ro \
```

Make sure to:
* Replace `$SSH_KEY` with a path to a SSH key that can be used to mirror the Git repositories
* Replace `$KNOWN_HOSTS` with a path to the known hosts file that trusts the remotes for the Git repositories

## Using HTTP(S)
To mirror repositories over HTTP(S), and to mirror LFS objects, add the following to the `docker run` command:

```bash
docker run ...
    -v $GIT_CREDENTIALS:/root/.git-credentials:ro \
```

Make sure to:
* Replace `$GIT_CREDENTIALS` with a path to the Git credentials file that can be used to mirror the Git (LFS) repositories  
    You can create this file using `git credential-store --file .git-credentials store` ([more information](https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage))  
    Keep in mind that credentials are stored **unencrypted** this way; consider using tokens

### Git LFS
To mirror LFS objects, add the following to the `docker run` command:

```bash
docker run ...
    -e GIT_MIRROR_USE_LFS=true \
```

# Troubleshooting
To help diagnosing issues, add the following to the `docker run` command:

```bash
docker run ...
    -e GIT_MIRROR_VERBOSE=true \
```
